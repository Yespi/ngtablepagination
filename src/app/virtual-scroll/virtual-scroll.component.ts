import { Component, OnInit } from '@angular/core';
import * as customData from '../data.json';

@Component({
  selector: 'app-virtual-scroll',
  templateUrl: './virtual-scroll.component.html',
  styleUrls: ['./virtual-scroll.component.css']
})
export class VirtualScrollComponent  {

  dataSource: any  = (customData  as  any).default;

  constructor() { 
  }

  onSubmit(id, status){
    alert('Submitted to API' + ' ' + 'Id:' + id + ' and ' + 'Status:' + status)
  }

}
