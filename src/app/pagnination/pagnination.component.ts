import { Component } from '@angular/core';
import * as customData from '../data.json';

@Component({
  selector: 'app-pagnination',
  templateUrl: './pagnination.component.html',
  styleUrls: ['./pagnination.component.css']
})
export class PagninationComponent {

  data: any  = (customData as any).default;
  startIndex = 0
  endIndex = 10
  pageSize = 10
  currentPage = 1
  pageCount = 0

  constructor(){
    this.pageCount = this.data.length / this.pageSize
    console.log('count:', this.pageCount)
  }
  selectPageCount(){
    this.pageCount = this.data.length / this.pageSize
    this.endIndex = this.pageSize
    this.startIndex = 0
    this.currentPage = 1
  }

  getPageSize(length): any{
    return new Array(length / this.pageSize)
 }

 changePage(index){
  this.currentPage = index + 1
  this.startIndex = index * this.pageSize
  this.endIndex = this.startIndex + this.pageSize
  this.pageCount = this.data.length / this.pageSize
  console.log('indexes: ', this.startIndex, this.endIndex, this.currentPage)
 }

 prevPage(){
  this.currentPage--
  this.startIndex = this.startIndex - this.pageSize
  this.endIndex = this.endIndex - this.pageSize
 }

 nextPage(){
  this.currentPage++ 
  this.startIndex = this.startIndex + this.pageSize
  this.endIndex = this.endIndex + this.pageSize
 }

 onSubmit(id, status){
  alert('Submitted to API' + ' ' + 'Id:' + id + ' and ' + 'Status:' + status)
 }

}
