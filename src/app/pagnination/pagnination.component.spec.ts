import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagninationComponent } from './pagnination.component';

describe('PagninationComponent', () => {
  let component: PagninationComponent;
  let fixture: ComponentFixture<PagninationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagninationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagninationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should individual page clicks navigate correctly', () => {
    const pageSize = 10
    component.selectPageCount()
    expect(component.endIndex).toEqual(pageSize)
  });

  it('should next page work', () => {
    component.currentPage = 5
    component.nextPage()
    expect(component.currentPage).toEqual(6)
  });

  it('should pre page work', () => {
    component.currentPage = 5
    component.prevPage()
    expect(component.currentPage).toEqual(4)
  });

});

